import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  mostrar = true;
  frase: any = {
    mensaje: 'hola soy un text',
    autor: 'Ben'
  };
  personajes: string[] = ['Francisco', 'Mario', 'Alejandro'];
  ngOnInit() {
  }
}
